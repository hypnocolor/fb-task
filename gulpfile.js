var $ = require('gulp-load-plugins')(),
	gulp = require('gulp'),
	csso = require('gulp-csso'),
	path = require('path'),
	pug = require('gulp-pug'),
	gulpif = require('gulp-if'),
	data = require('gulp-data'),
	portfinder = require('portfinder'),
	browserSync = require('browser-sync').create(),
	flatten = require('gulp-flatten'),
	fs = require('fs'),
	emitty = require('emitty').setup('app/pages', 'pug');

var assets = {
	styles: {
		dir: ['app/assets/styles'],
		src: 'app/assets/styles/main.sass',
		watch: [
			'app/assets/styles/**/*.sass',
			'app/assets/styles/**/*.scss',
			'app/blocks/**/*.sass',
			'app/pages/**/*.sass'
		],
		dest: 'dist/assets/css'
	},
	scripts: {
		src: [
			'app/assets/scripts/*.js',
			'app/blocks/**/*.js',
			'app/pages/**/*.js'
		],
		dest: 'dist/assets/js',
		vendors: [
			'app/assets/scripts/vendors/*.js',
			'app/assets/scripts/lib/*.js'
		]
	},
	images: {
		src: 'app/assets/images/**/*',
		dest: 'dist/assets/img'
	},
	fonts: {
		src: 'app/assets/fonts/**/*',
		dest: 'dist/assets/fonts'
	},
	dest: 'dist'
};

gulp.task('server', function(cb) {
	portfinder.getPort(function (err, port) {
		browserSync.init({
			startPath: '/index.html',
			server: {
				baseDir: assets.dest
			},
			notify: false,
			port: port
		}, cb);
	});
});

gulp.task('clean', function() {
	var del = require('del');
	return del([
		assets.dest,
		assets.fonts.dest,
		assets.images.dest,
		assets.scripts.dest,
		assets.styles.dest
	], {force:true});
});

gulp.task('templates', function() {

	return new Promise((resolve, reject) => {
		emitty.scan(global.emittyChangedFile).then(() => {
			gulp.src('app/pages/**/*.pug')
				.pipe(gulpif(global.watch, emitty.filter(global.emittyChangedFile)))
				.pipe(data(function(file) {
					return JSON.parse(fs.readFileSync('app/data.json'))
				}))
				.pipe($.plumber())
				.pipe(pug({ pretty: true }))
				.pipe(flatten())
				.pipe($.versionAppend(['html', 'js', 'css'], {appendType: 'guid', versionFile: 'version.json'}))
				.pipe(gulp.dest(assets.dest))
				.on('end', resolve)
				.on('error', reject)
				.pipe(browserSync.stream());
		});
	});
});

gulp.task('styles', function() {
	return gulp.src(assets.styles.src)
		.pipe($.plumber())
		.pipe($.sass({
			outputStyle: 'expanded',
			includePaths: [
				assets.styles.dir
			]
		}).on('error', $.sass.logError))
		.pipe($.postcss([
			require('autoprefixer')(),
			require('css-mqpacker')()
		]))
		.pipe(csso())
		.pipe(gulp.dest(assets.styles.dest))
		.pipe(browserSync.reload({ stream: true }));
});

gulp.task('scripts', function() {
	var src = [].concat(assets.scripts.vendors, assets.scripts.src);
	return gulp.src(src)
		.pipe($.plumber())
		.pipe($.concat('main.js'))
		.pipe($.uglify())
		.pipe(gulp.dest(assets.scripts.dest))
		.pipe(browserSync.stream());
});

gulp.task('images', function() {
	return gulp.src(assets.images.src)
		.pipe($.plumber())
		.pipe(gulp.dest(assets.images.dest))
		.pipe(browserSync.stream());
});

gulp.task('fonts', function() {
	return gulp.src(assets.fonts.src)
		.pipe($.plumber())
		.pipe(gulp.dest(assets.fonts.dest))
		.pipe(browserSync.stream());
});

gulp.task('watch', function() {
	global.watch = true;

	gulp.watch(['app/pages/**/*.pug', 'app/layout/**/*.pug'], gulp.series('templates'))
		.on('all', (event, filepath) => {
			global.emittyChangedFile = filepath;
		});

	gulp.watch(['app/data.json', 'app/blocks/**/*.pug'], gulp.series('templates'));

	gulp.watch(assets.styles.watch, gulp.series('styles'));
	gulp.watch(assets.images.src, gulp.series('images'));
	gulp.watch(assets.fonts.src, gulp.series('fonts'));
	gulp.watch([assets.scripts.src, 'app/assets/scripts/lib/*.js'], gulp.series('scripts'));
})

gulp.task('build', gulp.parallel('styles', 'scripts', 'images', 'fonts', 'templates'));
gulp.task('default', gulp.series('clean', 'build', 'server', 'watch'));