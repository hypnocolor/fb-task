Firstly you need to install [Node.js](https://nodejs.org/en/download/). Then install global modules:

```bash
$ npm i -g gulp
```

After that go to the project folder and install needed local dependencies:

```bash
$ npm i
```

Finally just run `gulp` in the project folder to build a project in `public` folder and run a local server with live preview that automatically opens in your default browser.
