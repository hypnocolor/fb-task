function makeMouseOutFn(elem) {
	var list = traverseChildren(elem);
	return function onMouseOut(event) {
		var e = event.toElement || event.relatedTarget;
		if (!!~list.indexOf(e)) {
			return;
		}
		if (elem.classList) {
			elem.classList.add('_hovered');
		} else {
			elem.className += ' ' + '_hovered';
		}
	};
}

function traverseChildren(elem) {
	var children = [];
	var q = [];
	q.push(elem);
	while (q.length > 0) {
		var elem = q.pop();
		children.push(elem);
		pushAll(elem.children);
	}
	function pushAll(elemArray) {
		for(var i=0; i < elemArray.length; i++) {
			q.push(elemArray[i]);
		}
	}
	return children;
}

document.addEventListener('DOMContentLoaded', function() {
	var productCards = document.querySelectorAll('.cat-card__outer');
	var over = 0, out = 0;

	[].forEach.call(productCards, function(el) {
		el.addEventListener('click', function() {
			var thisCheckbox = el.previousElementSibling;
			if(!thisCheckbox.checked) {
				if (el.classList) {
					el.classList.remove('_hovered');
				} else {
					el.className = el.className.replace(new RegExp('(^|\\b)' + '_hovered'.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
				}
			}
		});

		el.addEventListener('mouseout', makeMouseOutFn(el), true);
	});
});